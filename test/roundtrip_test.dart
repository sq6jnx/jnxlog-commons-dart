import 'package:clock/clock.dart';
import 'package:collection/collection.dart';
import 'package:jnxLog_commons/database.dart';
import 'package:jnxLog_commons/models.dart';
import 'package:test/test.dart';

void main() {
  DB db;
  setUp(() async {
    db = DB(null); // create in-memory database
  });

  group('Test whether objects after saving are identical to ones before saving',
      () {
    test('Profile', () {
      var profile1 = Profile.fromMap({
        'id': 1,
        'profileName': 'Test profile',
        'stationCallsign': 'SQ6JNX/P',
        'operator': 'SQ6JNX',
        'wwffLocation': 'none',
      });
      db.insert(profile1);
      var profile2 = db.getProfile(1);
      expect(true, MapEquality().equals(profile1.toMap(), profile2.toMap()));
    });
    test('QSO', () {
      var qso1 = QSO.fromMap({
        'id': 1,
        'timestamp': clock.now().toIso8601String(),
        'profileId': 0,
        'callsign': 'N0CALL',
        'band': '40M',
        'mode': 'CW',
        'rstRcvd': '569',
        'rstSent': '479',
        'isDeleted': true,
      });
      db.insert(qso1);

      var qso2 = db.getQso(1);
      expect(true, MapEquality().equals(qso1.toMap(), qso2.toMap()));
    });
  });
}
