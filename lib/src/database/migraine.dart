import 'dart:collection';

import 'package:sqlite3/sqlite3.dart';

abstract class Migraine {
  Database db;
  List<Function> steps;

  Migraine(Database db) {
    this.db = db;

    this.db.execute('''
    CREATE TABLE IF NOT EXISTS __migraine__ (
      id INT PRIMARY KEY
      , appliedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
      );
      ''');
  }

  int getLastMigrationIDApplied() {
    final query = db.prepare('''SELECT MAX(id) AS maxId FROM __migraine__;''');
    final result = query.select();

    return result.toList()[0]['maxId'];
  }

  void applyMigration(Function migration, int id) {
    migration(db);
    db.execute('''INSERT INTO __migraine__ (id) VALUES ($id);''');
  }

  void applyPendingMigrations() {
    final lastAppliedMigrationId = getLastMigrationIDApplied();
    final s = LinkedHashMap.from(steps.asMap());
    s.removeWhere((key, value) =>
        lastAppliedMigrationId != null && key <= lastAppliedMigrationId);
    s.forEach((id, migration) {
      applyMigration(migration, id);
    });
  }
}
