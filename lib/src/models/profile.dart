import 'dart:collection';

import 'package:jnxLog_commons/models.dart';
import 'package:meta/meta.dart';

import 'storable_object.dart';

class Profile extends StorableObject {
  int id;
  String profileName;
  String stationCallsign;
  String operator;
  String wwffLocation;

  Profile(
      {@required int id,
      @required String profileName,
      @required String stationCallsign,
      @required String operator,
      String wwff})
      : id = id,
        profileName = profileName,
        stationCallsign = stationCallsign,
        operator = operator,
        wwffLocation = wwff,
        super('profiles');

  Profile.fromMap(Map m)
      : id = m['id'],
        profileName = m['profileName'],
        stationCallsign = m['stationCallsign'],
        operator = m['operator'],
        wwffLocation = m['wwffLocation'],
        super('profiles');

  Profile.fromName(String profileName)
      : id = timeUID(),
        profileName = profileName,
        stationCallsign = '',
        operator = '',
        wwffLocation = null,
        super('profiles');
  @override
  LinkedHashMap<String, dynamic> toMap() {
    return LinkedHashMap.from({
      'id': id,
      'profileName': profileName,
      'stationCallsign': stationCallsign,
      'operator': operator,
      'wwffLocation': wwffLocation,
    });
  }
}
