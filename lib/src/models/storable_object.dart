import 'dart:collection';

abstract class StorableObject {
  final String tableName__;

  StorableObject(String tableName__) : tableName__ = tableName__;

  LinkedHashMap<String, dynamic> toMap();
}
